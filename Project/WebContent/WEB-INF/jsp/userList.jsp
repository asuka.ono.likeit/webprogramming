<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- オリジナルCSS読み込み -->
    <link href="css/common.css" rel="stylesheet">
</head>
<body>

	<header>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ユーザ管理システム</a>
      <ul class="navbar-nav px-3">
      <li class="navbar-text">${userInfo.name} さん </li>
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="LogoutServlet">ログアウト</a>
        </li>
      </ul>
    </nav>
  </header>

  <div class="container">


  <h1 class="display-4 text-center mt-5">ユーザ一覧</h1>

  <!-- 新規登録ボタン -->
    <div class="create-button-area">
      <a class="btn btn-outline-primary btn-lg" href="UserCreateServlet">新規登録</a>
    </div>

 	 <!-- 検索ボックス -->
    <div class="search-form-area">
      <div class="panel-body">
        <form method="post" action="UserListServlet" class="form-horizontal">
          <div class="form-group row">
            <label for="loginId" class="col-sm-4 col-form-label">ログインID</label>
            <div class="col-sm-12">
              <input type="text" name=loginId class="form-control" id="loginId">
            </div>
          </div>

          <div class="form-group row">
            <label for="userName" class="col-sm-4 col-form-label">ユーザ名</label>
            <div class="col-sm-12">
              <input type="text" name=userName class="form-control" id="userName">
            </div>
          </div>

          <div class="form-group row">

            <label for="birthDate" class="col-sm-4 col-form-label">生年月日</label>

            <div class="row col-sm-12">
              <div class="col-sm-5">
                <input type="date" name="date-start" id="date-start" placeholder="YYYY-MM-DD" class="form-control"/>
              </div>

              <div class="col-sm-2 text-center">
                ~
              </div>
              <div class="col-sm-5">
                <input type="date" name="date-end" id="date-end" placeholder="YYYY-MM-DD" class="form-control"/>
              </div>
            </div>
          </div>
          <div class="text-right">
            <button type="submit" value="検索" class="btn btn-primary form-submit">検索</button>
          </div>
        </form>
      </div>
    </div>




<table class="table text-center mt-5">
  <caption></caption>
  <thead class="thead-dark">
    <tr>
      <th>ログインID</th>
      <th>ユーザ名</th>
      <th>生年月日</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
   <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <c:if test="${userInfo.loginId == 'admin'}">
                     	 <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                     	 <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                      	 <a class="btn btn-danger" href ="UserDeleteConfServlet?id=${user.id}">削除</a>
                     </c:if>

  					 <c:if test="${userInfo.loginId != 'admin'}">
                     	 <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                     </c:if>

                     <c:if test="${userInfo.loginId == user.loginId}">
                     	 <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                     </c:if>
                     </td>
                   </tr>
                 </c:forEach>
  </tbody>
</table>
</div>

</body>
</html>