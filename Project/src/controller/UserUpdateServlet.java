package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		Object userInfo = session.getAttribute("userInfo");

		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
     	User userDetail = userDao.userDetail(id);

		// TODO  未実装：ユーザ情報をリクエストスコープにセット
		request.setAttribute("userDetail", userDetail);

		// ユーザ更新のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // リクエストパラメータの入力項目を取得
        String id = request.getParameter("id");
        String loginId = request.getParameter("loginId");
     	String password = request.getParameter("password");
    	String passwordConf = request.getParameter("passwordConf");
     	String userName = request.getParameter("userName");
     	String birthDate= request.getParameter("birthDate");


     	//更新失敗の場合
     	if(!password.equals(passwordConf) || userName.equals("") || birthDate.equals("")) {

	     	// リクエストスコープにエラーメッセージをセット
	     	request.setAttribute("errMsg", "入力した内容は正しくありません");

	     	// userCreate.jspにフォワード
	     	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
	     	dispatcher.forward(request, response);
	     	return;

     	}

     	//更新成功の場合

     	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
     	UserDao userDao = new UserDao();
     	User user = userDao.updateUserInfo(id,password,userName,birthDate);


     	// ユーザ一覧のサーブレットにリダイレクト
     		response.sendRedirect("UserListServlet");



     }

}


