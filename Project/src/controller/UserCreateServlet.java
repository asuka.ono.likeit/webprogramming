package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		Object userInfo = session.getAttribute("userInfo");

		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // リクエストパラメータの入力項目を取得
     	String loginId = request.getParameter("loginId");
     	String password = request.getParameter("password");
    	String passwordConf = request.getParameter("passwordConf");
     	String userName = request.getParameter("userName");
     	String birthDate= request.getParameter("birthDate");

     	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
     	UserDao userDao = new UserDao();
     	User user = userDao.allUserLoginId(loginId);

     	/** 登録できない場合 **/
		if (user != null || !password.equals(passwordConf) || loginId.equals("") ||
			password.equals("") || passwordConf.equals("") || userName.equals("") || birthDate.equals("")) {

		// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力した内容は正しくありません");

		// リクエストスコープに入力した値をセット
			request.setAttribute("loginId",loginId);
			request.setAttribute("userName",userName);
			request.setAttribute("birthDate",birthDate);

		// userCreate.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 登録できる場合 **/

		request.setAttribute("loginId", loginId);
		request.setAttribute("password", password);
		request.setAttribute("userName", userName);
		request.setAttribute("birthDate", birthDate);

		// UserAddServletにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("UserAddServlet");
			dispatcher.forward(request, response);
			return;

	}

}

