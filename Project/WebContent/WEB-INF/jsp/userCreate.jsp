<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ新規登録画面</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<header>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ユーザ管理システム</a>
      <ul class="navbar-nav px-3">
      <li class="navbar-text">${userInfo.name} さん </li>
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="LogoutServlet">ログアウト</a>
        </li>
      </ul>
    </nav>
  </header>


  <div class="container">

<h1 class="display-4 text-center mt-5 mb-5">ユーザ新規登録画面</h1>

<!-- 登録失敗した場合 -->
		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
		</c:if>



	 <form method="post" action="UserCreateServlet" class="form-horizontal text-right">
      <div class="form-group row">
        <label for="loginId" class="col-sm-4 col-form-label">ログインID</label>
        <div class="col-sm-8">
          <input type="text" name=loginId class="form-control w-50" id="loginId" value=${loginId}>
       </div>
      </div>

      <div class="form-group row">
        <label for="password" class="col-sm-4 col-form-label">パスワード</label>
        <div class="col-sm-8">
          <input type="password" name=password class="form-control w-50" id="password">
      </div>
     </div>

      <div class="form-group row ">
        <label for="passwordConf" class="col-sm-4 col-form-label text-nowrap">パスワード(確認)</label>
        <div class="col-sm-8">
          <input type="password" name=passwordConf class="form-control w-50" id="passwordConf">
      </div>
     </div>

      <div class="form-group row">
        <label for="userName" class="col-sm-4 col-form-label">ユーザ名</label>
        <div class="col-sm-8">
          <input type="text" name=userName class="form-control w-50" id="userName" value=${userName}>
      </div>
     </div>

      <div class="form-group row">
        <label for="birthDate" class="col-sm-4 col-form-label">生年月日</label>
        <div class="col-sm-8">
          <input type="date" name=birthDate class="form-control w-50" id="birthDate" placeholder="YYYY-MM-DD" value=${birthDate}>
      </div>
     </div>

	 <div class="row justify-content-center">
      <div class="submit-button-area">
        <button type="submit" value="検索" class="btn btn-primary btn-lg btn-block">登録</button>
      </div>
     </div>


      <div class="col-xs-4 text-left">
        <a href="UserListServlet">戻る</a>
      </div>

    </form>




  </div>


</body>
</html>