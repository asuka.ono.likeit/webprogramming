<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除画面</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

 <header>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ユーザ管理システム</a>
      <ul class="navbar-nav px-3">
      <li class="navbar-text">${userInfo.name} さん </li>
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="LogoutServlet">ログアウト</a>
        </li>
      </ul>
    </nav>
  </header>


<div class="container">

	<h1 class="display-4 text-center mt-5 mb-5">ユーザ削除確認画面</h1>

	<p class="h2 text-center">${loginId}さん</p>

	<p class="h2 text-center mb-5">本当に削除してもよろしいですか？</p>

      <div class="col-xs-4 text-center">
        	<a href="UserListServlet" class="btn btn-light">いいえ</a>
       		<a href="UserDeleteServlet?id=${id}" class="btn btn-light">はい</a>
      </div>


  </div>

</body>
</html>