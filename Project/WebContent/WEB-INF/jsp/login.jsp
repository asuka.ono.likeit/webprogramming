<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- オリジナルCSS読み込み -->
    <link href="css/login.css" rel="stylesheet">
</head>
<body>

	<form method="post" action="LoginServlet">
		<img class="mb-4" src="example.gif" alt="サンプル" width="100" height="100">
		<h1 class="h1 mb-3 font-weight-normal">ログイン画面</h1>


		<!-- ログインIDが存在しない場合、またはログインIDとパスワードの組み合わせが違う場合 -->
		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
		</c:if>

		<div class="form-group">
	   	 <label for="exampleInputLoginID">ログインID</label>
	  	 <input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID">
		</div>

  		<div class="form-group">
    		<label for="exampleInputPassword1">Password</label>
   			<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password">
  		</div>

  			<button type="submit" class="btn btn-primary">ログイン</button>

	</form>

</body>
</html>