<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ詳細画面</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

<header>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ユーザ管理システム</a>
      <ul class="navbar-nav px-3">
      <li class="navbar-text">${userInfo.name} さん </li>
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="LogoutServlet">ログアウト</a>
        </li>
      </ul>
    </nav>
  </header>

  <div class="container">

  	<h1 class="display-4 text-center mt-5 mb-5">ユーザ詳細画面</h1>

      <div class="form-group row">
        <label for="loginId" class="col-sm-5 col-form-label text-right">ログインID</label>
        <div class="col-sm-5">
          <p class="form-control-plaintext text-center">${userDetail.loginId}</p>
       </div>
      </div>

      <div class="form-group row">
        <label for="userName" class="col-sm-5 col-form-label text-right">ユーザ名</label>
        <div class="col-sm-5">
          <p class="form-control-plaintext text-center">${userDetail.name}</p>
      </div>
     </div>

      <div class="form-group row ">
        <label for="birthDate" class="col-sm-5 col-form-label text-nowrap text-right">生年月日</label>
        <div class="col-sm-5">
         <p class="form-control-plaintext text-center">${userDetail.birthDate}</p>
      </div>
     </div>

      <div class="form-group row">
        <label for="create_date" class="col-sm-5 col-form-label text-right">登録日時</label>
        <div class="col-sm-5">
          <p class="form-control-plaintext text-center">${userDetail.createDate}</p>
      </div>
     </div>

      <div class="form-group row">
        <label for="update_date" class="col-sm-5 col-form-label text-right ">更新日時</label>
        <div class="col-sm-5">
          <p class="form-control-plaintext text-center">${userDetail.updateDate}</p>
      </div>
     </div>


      <div class="col-xs-4">
        <a href="UserListServlet">戻る</a>
      </div>


  </div>




</body>
</html>